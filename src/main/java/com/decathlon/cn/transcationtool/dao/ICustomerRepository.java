package com.decathlon.cn.transcationtool.dao;

import com.decathlon.cn.transcationtool.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ICustomerRepository extends JpaRepository<Customer,Long> {

    Optional<Customer> findById(Long id);

    List<Customer> findByIdAndLoginAndMobile(Long id, String Login,String mobile);

    List<Customer> findByLoginAndMobile(String Login,String mobile);

    List<Customer> findByIdAndMobile(Long id,String mobile);

    List<Customer> findByIdAndLogin(Long id,String mobile);

    List<Customer> findByLogin(String login);

    List<Customer> findByMobile(String mobile);

    Customer findByLoginLike(String login);

    @Query("select c from Customer c where c.login=:login")
    List<Customer> queryByLogin(String login, String mobile);

    @Query("select c from Customer c where c.login=:login")
    List<Customer> queryByCondition(String login, String mobile);


}

//CrudRepository<Customer,Long>
