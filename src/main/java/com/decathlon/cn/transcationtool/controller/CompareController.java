package com.decathlon.cn.transcationtool.controller;

import com.decathlon.cn.transcationtool.Service.CustomerService;
import com.decathlon.cn.transcationtool.entity.ComparedObject;
import com.decathlon.cn.transcationtool.entity.Customer;
import com.decathlon.cn.transcationtool.entity.Transaction;
import com.decathlon.cn.transcationtool.utility.MyUtility;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "CompareController", description = "Webpos comparison tool")
@RequestMapping("/api/compareController/v1/*")
public class CompareController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ComparedObject comparedObject;

    @ApiOperation(value="retrieve compare result")
    @RequestMapping(value="compareResult", method= RequestMethod.GET)
    public ResponseEntity compareResult(@RequestParam String url){
        // Step1: read XML by URL
        String result = MyUtility.readFromWebpos(url);
        Transaction transaction = MyUtility.readDataFromString(result);


        // Step2: retrieve data from database based on transcationid
        long id=1;
        Customer customer = customerService.findById(id);

        // Step3: compare two value


        // Step4: assemble result object

        return null;
    }

    @ApiOperation(value="update compared fields")
    @RequestMapping(value="comparedObject", method= RequestMethod.PUT,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity comparedFields(@RequestBody ComparedObject fieldObject){
        ComparedObject co = MyUtility.setOmparedFields(fieldObject);
        return co == null ? new ResponseEntity("500:Internal Server error" , HttpStatus.OK) : new ResponseEntity(HttpStatus.OK);
    }

    @ApiOperation(value="read compared fields")
    @RequestMapping(value="comparedObject", method= RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> comparedFields(){
        return MyUtility.comparedFields;
    }

    /**
     * for insert test data
     * @param customer
     * @return
     */
    @ApiOperation(value="create customer", notes="This API is only for test and insert test data to database")
    @RequestMapping(value="customer",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Customer> customer(@RequestBody Customer customer){
        customerService.save(customer);
        return new ResponseEntity(customer,HttpStatus.OK);
    }



    @ApiOperation(value="query customer",notes="This API is to retrieve customer")
    @RequestMapping(value="customer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams(
            {       @ApiImplicitParam(paramType = "query", dataType = "Long", name = "id"),
                    @ApiImplicitParam(paramType = "query", dataType = "String", name = "login"),
             @ApiImplicitParam(paramType = "query", dataType = "String", name = "mobile")}

    )
    public ResponseEntity<List<Customer>> customer(Long id, String login,String mobile){
        List<Customer> cl = null;
        if(id !=null && login != null && mobile != null){
            cl = customerService.findByIdAndLoginAndMobile(id, login, mobile);
        }
        else if(login != null && mobile != null){
            cl = customerService.findByLoginAndMobile(login, mobile);
        }
        else if(id !=null && login !=null){
            cl = customerService.findByIdAndLogin(id, login);
        }
        else if(id != null && mobile != null){
            cl = customerService.findByIdAndMobile(id, mobile);
        }
        else if(login !=null){
            cl = customerService.findByLogin(login);
        }
        else if(mobile !=null){
            cl = customerService.findByMobile(mobile);
        }
        else if(id != null){
            cl = new ArrayList<>();
            Customer customer = customerService.findById(id);
            if(customer!=null)
                cl.add(customer);
        }
        else
            cl = customerService.findAll();
        return new ResponseEntity(cl,HttpStatus.OK);
    }
}
