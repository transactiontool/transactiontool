package com.decathlon.cn.transcationtool.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Data
@Component
@PropertySource("classpath:comparedObject.properties")
public class ComparedObject {

    @Value("${comparedObject.fields}")
    private String fields;

    public List<String> getComparedField(){
        String[] fieldArray = fields.split(";");
        return fieldArray==null?null:Arrays.asList(fieldArray);
    }


}
