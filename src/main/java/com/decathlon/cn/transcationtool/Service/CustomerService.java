package com.decathlon.cn.transcationtool.Service;

import com.decathlon.cn.transcationtool.dao.ICustomerRepository;
import com.decathlon.cn.transcationtool.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CustomerService {

    @Autowired
    private ICustomerRepository customerRepository;

    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    // modify this method
    public Customer findById(Long id) {
        Optional<Customer> cus = customerRepository.findById(id);
        return cus.isPresent()? cus.get(): null;
    }

    public List<Customer> findByLogin(String login){
        return customerRepository.findByLogin(login);
    }

    public List<Customer> findByMobile(String mobile){
        return customerRepository.findByMobile(mobile);
    }

    public List<Customer> findByLoginAndMobile(String login,String mobile){
        return customerRepository.findByLoginAndMobile(login, mobile);
    }

    public List<Customer> findByIdAndLoginAndMobile(Long id,String login,String mobile){
        return customerRepository.findByIdAndLoginAndMobile(id,login, mobile);
    }

    public List<Customer> findByIdAndLogin(Long id,String login){
        return customerRepository.findByIdAndLogin(id, login);
    }

    public List<Customer> findByIdAndMobile(Long id,String mobile){
        return customerRepository.findByIdAndMobile(id, mobile);
    }

    public void save(Customer entity) {
        customerRepository.save(entity);
    }

}
