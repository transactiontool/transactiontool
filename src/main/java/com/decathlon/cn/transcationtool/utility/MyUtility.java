package com.decathlon.cn.transcationtool.utility;

import com.decathlon.cn.transcationtool.entity.ComparedObject;
import com.decathlon.cn.transcationtool.entity.Transaction;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.dom4j.Attribute;
import org.dom4j.io.SAXReader;
import org.jdom2.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Component
public class MyUtility {

    @Autowired
    private ComparedObject comparedObject;

    public static List<String> comparedFields ;

    @PostConstruct
    public void init() {
        MyUtility.comparedFields = comparedObject.getComparedField();
    }

    /**
     *
     * @param url
     * @return
     */
    public static String readFromWebpos(String url) {
        // method1:
        String result1 = readFromWebpos_RestTemplate(url);

        // method2:
        String result2 = readFromWebpos_HttpClients(url);

        return result2;
    }

    public static Transaction readDataFromString(String content) {
        // method1:
        System.out.println("--------------------Dom4j----------------------");
        StringBuffer content_times = new StringBuffer();
        for(int i=0;i<1000;i++){
            content_times = content_times.append(content);
        }
        long startTime = System.currentTimeMillis();
        Transaction t1 = readDataFromString_DOM4j(content); //suitable for small file, generate and modify a file
        long endtime = System.currentTimeMillis();
        System.err.println("耗时："+(endtime-startTime)/1000+"seconds");
        System.out.println("--------------------Dom4j END----------------------");
        //method2:

        System.out.println("--------------------JDom----------------------");
        Transaction t2 = readDataFromString_JDOM(content);
        System.out.println("--------------------JDom END----------------------");

        // method3:
        System.out.println("--------------------Dom----------------------");
        Transaction t3 = readDataFromString_DOM(content);
        System.out.println("--------------------Dom END----------------------");
        // method4:
        System.out.println("--------------------SAX----------------------");
        Transaction t4 = readDataFromString_SAX(content); // save memory
        System.out.println("--------------------SAX END----------------------");
        return t1;
    }

    public static Transaction readObjectFromWebpos(String url) {
        Transaction t = new Transaction();
        return t;
    }

    public static String readFromWebpos_RestTemplate(String url) {
//        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
//        requestFactory.setConnectTimeout(1000);// 设置超时
//        requestFactory.setReadTimeout(1000);

        System.out.println("------------------RestTemplate--------------------");
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url,String.class);

        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        String resultget = restTemplate.getForObject(url, String.class);
        System.out.println("Response content: " + resultget);
        System.out.println("------------------RestTemplate End--------------------");
        return resultget;
    }

    public static String readFromWebpos_HttpClients(String url) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet httpget = new HttpGet(url);
            System.out.println("executing request " + httpget.getURI());
            CloseableHttpResponse response = httpclient.execute(httpget);
            try {
                HttpEntity entity = response.getEntity();
                System.out.println("------------------HttpClient--------------------");
                System.out.println(response.getStatusLine());
                String result = EntityUtils.toString(entity);
                if (entity != null) {
                    System.out.println("Response content length: " + entity.getContentLength());
                    System.out.println("Response content: " + result);
                }
                System.out.println("-----------------HttpClient End-------------------");
                return result;
            } finally {
                response.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";

    }

    public static Transaction readDataFromString_DOM(String content) {
        Transaction t = new Transaction();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document doc = null;
        InputStream inputStream = null;
        try {
            builder = factory.newDocumentBuilder();
            inputStream = new ByteArrayInputStream(content.getBytes("UTF-8"));
            doc = builder.parse(inputStream);


            NodeList details = doc.getElementsByTagName("RetailTransaction");
            for(int index = 0; index < details.getLength(); index ++){
                Node node = details.item(index);
                if(node.hasAttributes()){
                    NamedNodeMap map = node.getAttributes();
                    System.out.println("name:" + node.getNodeName() +": value=" + node.getNodeValue());
                    Node nn = map.getNamedItem("TransactionStatus");
                    System.out.println("name:" + nn.getNodeName() +": value=" + nn.getNodeValue());
                }
            }

        } catch (Exception e) {
            return null;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return t;

    }

    public static Transaction readDataFromString_JDOM(String content) {
        Transaction t = new Transaction();
        try {

            InputSource source = new InputSource(new ByteArrayInputStream(content.getBytes("UTF-8")));
            SAXBuilder saxbBuilder = new SAXBuilder();
            org.jdom2.Document doc = saxbBuilder.build(source);
            org.jdom2.Element root = doc.getRootElement();
            System.out.println(root.getName());
            List<?> node = root.getChildren();
            for (int i = 0;
                 i < node.size();
                 i++) {
                org.jdom2.Element element = (org.jdom2.Element) node.get(i);
                System.out.println("node infos: " + element.getName() + ":" + element.getText());
                List<?> subNode = element.getChildren();
                for (int j = 0;
                     j < subNode.size();
                     j++) {
                    org.jdom2.Element subElement = (org.jdom2.Element) subNode.get(j);
                    System.out.println("sub nodes: " + subElement.getName() + ":" + element.getText());

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;

    }

    public static Transaction readDataFromString_SAX(String content) {
        Transaction t = new Transaction();
        javax.xml.parsers.SAXParserFactory factory = javax.xml.parsers.SAXParserFactory.newInstance();
        try {

            javax.xml.parsers.SAXParser parser = factory.newSAXParser();
            org.xml.sax.XMLReader reader = parser.getXMLReader();
            reader.setContentHandler(new DefaultHandler() {
                @Override
                public void startDocument() throws SAXException {
                    System.out.println("1 开始读取文档了....");
                }

                @Override
                public void endDocument() throws SAXException {
                    System.out.println("10 文档读取结束了....");
                }

                @Override
                public void startElement(String uri, String localName, String qName, org.xml.sax.Attributes attributes)
                        throws SAXException {
                    System.out.println("一个元素开始....");
                    int length = attributes.getLength();
                    for(int i= 0; i< length; i++){
                        System.out.println("qname: "+attributes.getQName(i)+"; value="+attributes.getValue(i));
                    }

                }

                @Override
                public void endElement(String uri, String localName, String qName) throws SAXException {
                    System.out.println("一个元素结束....");
                }
            });

            reader.parse(new InputSource(new StringReader(content)));
            System.out.println("Over.....");
        } catch (Exception e) {

        }
        return t;

    }

    public static Transaction readDataFromString_DOM4j(String content) {
        Transaction t = new Transaction();
        SAXReader saxReader = new SAXReader();
        InputStream inputStream = null;
        try {
            inputStream = new ByteArrayInputStream(content.getBytes("UTF-8"));
            org.dom4j.Document document = saxReader.read(inputStream);
            org.dom4j.Element ele = document.getRootElement();
            parserNode(ele);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return t;

    }

    public static void parserNode(org.dom4j.Element ele) {
        // value of element
        if(MyUtility.comparedFields.contains(ele.getName())){
            System.out.println(ele.getName() + ":" + ele.getText().trim());
        }
        //value of attribute
        List<Attribute> attrList = ele.attributes();
        for (Attribute attr : attrList) {
            String name = attr.getName();
            String value = attr.getValue();
            if(!MyUtility.comparedFields.contains(name)){
               continue;
            }
            System.out.println(name + "=" + value);
        }

        List<org.dom4j.Element> eleList = ele.elements();
        for (org.dom4j.Element e : eleList) {
            parserNode(e);
        }
    }

    public static synchronized ComparedObject setOmparedFields(ComparedObject co){
        MyUtility.comparedFields = co.getComparedField();
        return co;
    }

}
